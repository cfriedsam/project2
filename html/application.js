// Main object handling all interactions between
// web page, maps, BART API and local backend files
var Bart = {
  // template for each route element inserted into dropdown
  route_template: '<li class="route" id="ID"><div class="icon" style="background-color:COLOR"></div>NAME</li>',
  // template for Google Maps InfoWindow to display station information
  marker_template: '<div class="title">Station: STATION</div><strong>Arrivals / Departures</strong><div id="departures">Loading ...</div><strong>Other Trains</strong><div id="other_trains">Loading ...</div>',
  // reference to our map object
  map: null,
  // reference to xml with route information
  routes: null,
  // reference to xml with routeinfo information
  routeinfo: null,
  // reference to xml for stationinfo
  stationinfo: null,
  // reference holding information about the current selected route
  selected_route: null,
  // reference holding information about currently selected stations
  selected_stations: null,
  // reference to our current polyline object displayed on map so we can remove it
  current_polyline: null,
  // reference to markers currently displayed on map so we can remove them
  current_markers: null,
  // reference to info window object
  info_window: null,
  // holds debug information, helpful when trying to figure out what information
  // was returned from API calls
  debug: null,
  
  // initialize callbacks based on elements on page
  init: function() {
    if ($('#map_canvas')) {
      Bart.init_map();
      Bart.init_callbacks();
      Bart.load_data();
    };
  },

  // creates a new map object centered to San Francisco
  // creates info window object used to display station information
  init_map: function() {
    var mapOptions = {
      center: new google.maps.LatLng(37.774921, -122.419453),
      zoom: 12,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    Bart.map = new google.maps.Map($('#map_canvas')[0], mapOptions);
    Bart.info_window = new google.maps.InfoWindow({
      content: 'Hello World'
    });
  },
  
  // binds callback to #routes so we can update route selection
  // when the user clicks on any of the displayed routes
  init_callbacks: function() {
    if ($('#routes')) {
      $('#routes').bind('click', Bart.update_route_selection);
    }
  },

  // pre-loads all backend xml files so we have them
  // available once needed and sets routes up
  load_data: function() {
    // load routes
    $.ajax({
      method: 'GET',
      url: '/routes.xml',
      dataType: 'xml',
      success: function(xml) {
        Bart.routes = $(xml);
        Bart.init_routes();
      }
    });
    
    // load routeinfo
    $.ajax({
      method: 'GET',
      url: '/routeinfo.xml',
      dataType: 'xml',
      success: function(xml) {
        Bart.routeinfo = $(xml);
      }
    });
    
    // load stations
    $.ajax({
      method: 'GET',
      url: '/stationinfo.xml',
      dataType: 'xml',
      success: function(xml) {
        Bart.stationinfo = $(xml);
      }
    });
  },

  // populates routes dropdown with information
  // from routes xml file
  init_routes: function(xml) {
    var route = $('#routes-ul');
    var id, name, color;
    Bart.routes.find('route').each(function() {
      if (typeof(this) != undefined) {
        id = $(this).find('routeID').text();
        name = $(this).find('name').text();
        color = $(this).find('color').text();
        
        route.append(
          Bart.route_template.replace('ID', id).
          replace('NAME', name).
          replace('COLOR', color)
        );
      }
    });
  },
  
  // called when user selects a route from dropdown
  // calls draw_route with routeID
  update_route_selection: function(e) {
    var e = e || window.event;
    var target = e.target;
    if (target.className == 'route') {
      Bart.draw_route(target.id);
    }
  },
  
  // gets stations for selected route
  // calls draw_polyline_and_markers to update map overlay
  draw_route: function(routeId) {
    // select the route selected
    var route = Bart.routeinfo.find("route").filter(function() {
      return $(this).find('routeID').text() == routeId;
    });
    Bart.selected_route = route;
    
    // filter all stations for this route
    var stations = $(route).find('station').map(function(index, station) {
      var abbr = $(station).text();
      return Bart.stationinfo.find("station").filter(function() {
        return $(this).find('abbr').text() == abbr;
      });
    });
    Bart.selected_stations = stations;
    
    // draw polyline on map
    Bart.draw_polyline_and_markers();
  },
  
  // clears existing makers and polyline
  // draws new markers and polyline based on selected stations
  draw_polyline_and_markers: function() {
    if (Bart.selected_stations) {
      Bart.clear_polyline_and_markers();
      
      Bart.current_markers = [];
      var image = new google.maps.MarkerImage(
        'bart.png',
        new google.maps.Size(24, 15),
        new google.maps.Point(0, 0),
        new google.maps.Point(12, 7.5)
      );
      var waypoints = Bart.selected_stations.map(function(index, station) {
        var lat = $(station).find('gtfs_latitude').text();
        var lng = $(station).find('gtfs_longitude').text();
        var latlng = new google.maps.LatLng(lat, lng);

        // draw marker
        var marker = new google.maps.Marker({
          position: latlng,
          map: Bart.map,
          icon: image
        });
        marker.set('data', JSON.stringify({
          'station': $(station).find('name').text(),
          'id': $(station).find('abbr').text()
        }));
        google.maps.event.addListener(marker, 'click', function() {
          Bart.display_info_window(marker);
        });
        Bart.current_markers.push(marker);
        
        return latlng;
      });
      
      Bart.current_polyline = new google.maps.Polyline({
        path: waypoints,
        strokeColor: Bart.selected_route.find('color').text(),
        strokeWeight: 2
      });
      
      Bart.current_polyline.setMap(Bart.map);
    }
  },
  
  // removes all polylines and markers from map
  clear_polyline_and_markers: function() {
    if (Bart.current_polyline) {
      Bart.current_polyline.setMap(null);
      Bart.current_polyline = null;
    }
    if (Bart.current_markers) {
      $(Bart.current_markers).each(function(index, marker) {
        marker.setMap(null);
      });
      Bart.current_markers = null;
    }
  },
  
  // updates and displays info window
  // calls load_arrivals_and_departures to get information
  // from BART API
  display_info_window: function(marker) {
    var data = $.parseJSON(marker.data);
    var content = Bart.marker_template.replace('STATION', data.station);
    Bart.info_window.setContent(content);
    Bart.info_window.open(Bart.map, marker);
    Bart.load_arrivals_and_departures(data.id);
  },
  
  // makes call to BART API with station
  // calls display_station_information if call is successful
  // shows error message otherwise
  load_arrivals_and_departures: function(station) {
    $.ajax({
      method: 'GET',
      url: 'http://api.bart.gov/api/etd.aspx?cmd=etd&orig='+station+'&key=MW9S-E7SL-26DU-VV8V',
      dataType: 'xml',
      success: Bart.display_station_information,
      failure: function() { alert('Sorry, failed to load information'); }
    });
  },
  
  // updates info window with information retreived
  // from BART API
  display_station_information: function(xml) {
    var direction = Bart.selected_route.find('abbr').text().split('-')[1];
    Bart.debug = xml;

    // selected direction
    var etd = $(xml).find('etd').filter(function() {
      return $(this).find('abbreviation').text() == direction;
    });

    var content = [];
    $(etd).find('estimate').each(function(index, estimate) {
      var minutes  = $(estimate).find('minutes').text();
      var platform = $(estimate).find('platform').text();
      content.push('<li>' + minutes + ' min on platform ' + platform + '</li>');
    });
    if (content.length == 0) {
      $('#departures').html('Sorry, there are no trains scheduled at this time.');
    } else {
      $('#departures').html('<ul>'+content.join('')+'</ul>');
    }
    
    // other directions
    var etd = $(xml).find('etd').filter(function() {
      return $(this).find('abbreviation').text() != direction;
    });
    
    var other_trains = [];
    $(etd).each(function(index, etd) {
      var direction = $(etd).find('destination').text();
      var color     = $(etd).find('hexcolor').first().text();
      
      var content = [];
      $(etd).find('estimate').each(function(index, estimate) {
        var minutes  = $(estimate).find('minutes').text();
        var platform = $(estimate).find('platform').text();
        content.push('<li>' + minutes + ' min on platform ' + platform + '</li>');
      });
      if (content.length > 0) {
        other_trains.push('<div><div class="icon" style="background-color:' + color + '"></div><strong>' + direction + '</strong><ul>' + content.join('') + '</ul></div>');
      }
    });
    if (other_trains.length > 0) {
      $('#other_trains').html(other_trains.join(''));
    } else {
      $('#other_trains').html('Sorry, no other trains scheduled at this time.');
    }
  }
}

// initializes Bart object once page has loaded
$(document).ready(Bart.init);
